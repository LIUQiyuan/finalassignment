import sqlite3
import openpyxl

wb = openpyxl.load_workbook("World Temperature.xlsx")
wb.create_sheet(index=0,title="Temperature by city")
sheet1 = wb.get_sheet_by_name("Temperature by city")
print("excel done")
connection = sqlite3.connect("Temperatures.db")
cursor = connection.cursor()

cursor.execute("""drop table if exists AvgTemp""")
sql_command1 = """
CREATE TABLE AvgTemp(
Date DATE,
Temp REAL,
City TEXT
);"""
cursor.execute(sql_command1)
print("1done")

sql_command2 = """
SELECT Date, AverageTemperature, City
FROM city
WHERE Country = 'China'
ORDER BY City, Date
"""
cursor.execute(sql_command2)
print("2 done")

results = cursor.fetchall()
for r in results:
    Date = r[0]
    Temp = r[1]
    City = r[2]
    sql_command3 = """
    INSERT INTO AvgTemp
    VALUES ("{Date}", "{Temp}", "{City}");
    """
    x = sql_command3.format(Date=Date, Temp=Temp, City=City)
    cursor.execute(x)
print("insert table done")

sql_command4 = """
SELECT SUM(Temp) AS whole, COUNT(Temp) AS count, City, strftime('%Y', Date) AS Year
FROM AvgTemp
GROUP BY City, Year
"""
cursor.execute(sql_command4)
results2 = cursor.fetchall()
print("3 done")
sheet1.cell(row=1, column=1).value = "AvgTemp"
sheet1.cell(row=1,column=2).value = "City"
sheet1.cell(row=1,column=3).value = "Year"
for i in range(1, len(results2)):
    sheet1.cell(row=i + 1, column=1).value = results2[i][0] / results2[i][1]
    sheet1.cell(row=i + 1, column=2).value = results2[i][2]
    sheet1.cell(row=i + 1, column=3).value = results2[i][3]
print("4 done")

Max_Row = sheet1.max_row
Max_Column = sheet1.max_column
for i in range(1, Max_Row+1):
    print(str(sheet1.cell(row=i,column=1).value) + ',' + str(sheet1.cell(row=i,column=2).value) + ','
          + str(sheet1.cell(row=i, column=3).value))

wb.save("World Temperature.xlsx")
wb.close()