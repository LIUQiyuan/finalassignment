import sqlite3
import openpyxl

# open a worksheet
wb1 = openpyxl.load_workbook('GlobalLandTemperaturesByCountry.xlsx')
sheet1 = wb1.get_sheet_by_name("GlobalLandTemperaturesByCountry")
wb2 = openpyxl.load_workbook('GlobalLandTemperaturesByMajorCity.xlsx')
sheet2 = wb2.get_sheet_by_name("GlobalLandTemperaturesByMajorCi")
wb3 = openpyxl.load_workbook('GlobalLandTemperaturesByState.xlsx')
sheet3 = wb3.get_sheet_by_name("GlobalLandTemperaturesByState")

# Connect to the database
connection = sqlite3.connect("Temperatures.db")
cursor = connection.cursor()

# Create a new corresponding table
sql_command1 = """
CREATE TABLE country(
Date DATE,
AverageTemperature DOUBLE,
AverageTemperatureUncertainty DOUBLE,
Country VARCHAR(30)); """
# import data from the worksheet to the table
cursor.execute(sql_command1)
for r in range(2, sheet1.max_row):
    Date = sheet1.cell(row=r, column=1).value
    AverageTemperature = sheet1.cell(row=r, column=2).value
    AverageTemperatureUncertainty = sheet1.cell(row=r, column=3).value
    Country = sheet1.cell(row=r, column=4).value
    country_data = """
    INSERT INTO country (Date, AverageTemperature, AverageTemperatureUncertainty, Country) 
    VALUES('""" + str(Date) + "','" + str(AverageTemperature) + "','" + str(AverageTemperatureUncertainty) \
                   + "',\"" + str(Country) + "\")"
    cursor.execute(country_data)

# Create a new corresponding table
sql_command2 = """
CREATE TABLE city(
Date DATE,
AverageTemperature DOUBLE,
AverageTemperatureUncertainty DOUBLE,
City CHAR(30),
Country CHAR(30),
latitude CHAR(20),
longitude CHAR(20));"""
cursor.execute(sql_command2)
# import data from the worksheet to the table
for r in range(2, sheet2.max_row):
    Date2 = sheet2.cell(row=r, column=1).value
    AverageTemperature2 = sheet2.cell(row=r, column=2).value
    AverageTemperatureUncertainty2 = sheet2.cell(row=r, column=3).value
    City2 = sheet2.cell(row= r, column=4).value
    Country2 = sheet2.cell(row=r, column=5).value
    latitude = sheet2.cell(row=r, column=6).value
    longitude = sheet2.cell(row=r, column=7).value
    city_data = """
    INSERT INTO city (Date, AverageTemperature, AverageTemperatureUncertainty, City, Country, latitude, longitude) 
    VALUES('""" + str(Date2) + "','" + str(AverageTemperature2) + "','" + str(AverageTemperatureUncertainty2) \
                + "','" + str(City2) + "',\"" + str(Country2) + "\",'" + str(latitude) + "','" + str(longitude) + "')"
    cursor.execute(city_data)

# Create a new corresponding table
sql_command3 = """
CREATE TABLE state(
Date DATE,
AverageTemperature DOUBLE,
AverageTemperatureUncertainty DOUBLE,
State CHAR(30),
Country CHAR(30)); """
cursor.execute(sql_command3)
# import data from the worksheet to the table
for r in range(2, sheet3.max_row):
    Date3 = sheet3.cell(row=r, column=1).value
    AverageTemperature3 = sheet3.cell(row=r, column=2).value
    AverageTemperatureUncertainty3 = sheet3.cell(row=r, column=3).value
    State3 = sheet3.cell(row=r, column=4).value
    Country3 = sheet3.cell(row=r, column=5).value
    state_data = """
    INSERT INTO state(Date, AverageTemperature, AverageTemperatureUncertainty, State, Country)
    VALUES('""" + str(Date3) + "','" + str(AverageTemperature3) + "','" + str(AverageTemperatureUncertainty3) \
                + "',\"" + str(State3) + "\",\"" + str(Country3) + "\")"
    cursor.execute(state_data)
# Commit the change and close the connection
cursor.close()
connection.commit()
connection.close()
print("done")