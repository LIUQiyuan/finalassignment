Data and file overview:
There are four python files:
1.db_create.py: In this file, a database named Temperatures is created and three tables which are created manually in it.
2.sql_temp.py: Information of southerncities are selected and stored into a new database table. 
The maximum, minimum and average temperature data of Queensland for year 2000 and print this information to the console.
3.excel_temp.py: A new workbook and a worksheet are created in this file.
In this file, the yearly mean temperature of each city in China are calculated and the relevant data are writen into the worksheet just created.

In the end, show the data in a line chart.

4.numpy_temp.py: Calculate the mean temperature of each Australian state and Australia.
Calculate the differences between each state and the national data for each year. 
Finally, write the data into a worksheet and show the differences in a graph.