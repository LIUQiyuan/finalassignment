import sqlite3

connection = sqlite3.connect('Temperatures.db')
cursor = connection.cursor()
cursor.execute("drop table if exists SouthernCities")
# Create a table named SouthernCities
sql_command1 = """
CREATE TABLE SouthernCities(
city TEXT,
country TEXT,
latitude TEXT,
longitude TEXT);"""
cursor.execute(sql_command1)
print("ok1")

# Find major cities located in southern hemisphere
sql_command2 = """
SELECT DISTINCT city, country, latitude, longitude
FROM city
WHERE latitude LIKE '%S'
ORDER BY Country;
"""
cursor.execute(sql_command2)
print("ok2")

results = cursor.fetchall()
for r in results:
    city = r[0]
    country = r[1]
    latitude = r[2]
    longitude = r[3]
    sql_command3 = """
    INSERT INTO SouthernCities
    VALUES("{city}", "{country}", "{latitude}", "{longitude}");
    """
    y = sql_command3.format(city=city, country=country, latitude=latitude, longitude=longitude)
    cursor.execute(y)
print("ok3")
sql_command4 = """
SELECT MAX(AverageTemperature), MIN(AverageTemperature), AVG(AverageTemperature)
FROM state
WHERE Date LIKE '2010%' and state = 'Queensland' 
"""
cursor.execute(sql_command4)
results2 = cursor.fetchall()
for i in results2:
    print(i)
