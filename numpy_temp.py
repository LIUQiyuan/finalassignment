import openpyxl
import sqlite3

wb = openpyxl.load_workbook("World Temperature.xlsx")
wb.create_sheet(index=1, title="Comparison")
sheet1 = wb.get_sheet_by_name("Comparison")
sheet1 = wb.active
connection = sqlite3.connect("Temperatures.db")
cursor = connection.cursor()

cursor.execute("""drop table if exists AvgTempOfState""")
sql_command1 = """
CREATE TABLE AvgTempOfState(
Date DATE,
Temp REAL,
State TEXT
);"""
cursor.execute(sql_command1)
sql_command2 = """
SELECT Date, AverageTemperature, State
FROM state
WHERE Country = "Australia"
ORDER BY State, Date;
"""
cursor.execute(sql_command2)
print("Select done")
result1 = cursor.fetchall()
for i in result1:
    Date = i[0]
    Temp = i[1]
    State = i[2]
    sql_command3 = """
    INSERT INTO AvgTempOfState
    VALUES ("{Date}", "{Temp}", "{State}");
    """
    x = sql_command3.format(Date=Date, Temp=Temp, State=State)
    cursor.execute(x)
print("Insert1 done")

sql_command4 = """
SELECT SUM(Temp) AS whole, COUNT(Temp) AS count, State, strftime('%Y', Date) AS Year
FROM AvgTempOfState
GROUP BY State, Year; 
"""
cursor.execute(sql_command4)
result2 = cursor.fetchall()
stateTemp = list()
stateYear = list()
sheet1.cell(row=1,column=1).value = "AverageTemperature"
sheet1.cell(row=1,column=2).value = "State"
sheet1.cell(row=1,column=3).value = "Year"
for i in range(1, len(result2)):
    sheet1.cell(row=i + 1, column=1).value = result2[i][1]
    sheet1.cell(row=i + 1, column=2).value = result2[i][2]
    sheet1.cell(row=i + 1, column=3).value = result2[i][3]
    stateTemp.append(result2[i][1])
    stateYear.append(result2[i][3])
print("Insert2 done")

cursor.execute("""drop table if exists AvgTempByAustralia""")
sql_command5 = """
CREATE TABLE AvgTempOfAustralia
(
date TEXT,
temp REAL,
country TEXT
);
"""
cursor.execute(sql_command5)
sql_command6 = """
SELECT Date, AverageTemperature, Country
FROM country
WHERE Country = "Australia"
ORDER BY Date;
"""
cursor.execute(sql_command6)
print("ok1")
result3 = cursor.fetchall()
for n in result3:
    Date = n[0]
    Temp = n[1]
    Country = n[2]
    insertCountry = """
    INSERT INTO AvgTempOfAustralia
    VALUES ("{Date}", "{Temp}", "{Country}");
    """
    y = insertCountry.format(Date=Date, Temp=Temp, Country=Country)
    cursor.execute(y)
Aus_Temp = """
SELECT SUM(Temp) AS whole, COUNT(Temp) AS count, strftime('%Y', Date) AS Year, Country
FROM AvgTempOfAustralia
GROUP BY Year; 
"""
cursor.execute(Aus_Temp)
result4 = cursor.fetchall()
max_row = sheet1.max_row
AusYear = list()
AusTemp = list()
for m in range(1, len(result4)):
    sheet1.cell(row=sheet1.max_row + i, column=1).value = result4[i][2]
    sheet1.cell(row=max_row + i, column=2).value = result4[i][1]
    sheet1.cell(row=max_row + i, column=3).value = " "
    sheet1.cell(row=max_row + i, column=4).value = result4[i][3]
    AusYear.append(result4[i][2])
    AusTemp.append(result4[i][1])
print("I can't do this one.")
